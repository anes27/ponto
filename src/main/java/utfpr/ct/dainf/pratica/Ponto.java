package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;
    
    
    /**
     * Construtor padrão
     */
    public Ponto() {}
      
    public Ponto(double x, double y, double z) {
        this.x=x;
        this.y=y;
        this.z=z;
    }
    
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
    public double dist(Ponto p) {
        return Math.sqrt(Math.pow((p.getX()-this.x),2)+Math.pow((p.getY()-this.y),2)+Math.pow((p.getZ()-this.z),2));
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    @Override
    public String toString() {
        return String.format(getNome()+"("+getX()+","+getY()+","+getZ()+")");
    }
    
    @Override
    public boolean equals(Object o) {
        boolean result = false;
        
        if(o==null) {
            return false;
        } else if(o.getClass() != this.getClass()) {
            
        } else if(this.x == ((Ponto)o).getX() &&
           this.y == ((Ponto)o).getY() &&
           this.x == ((Ponto)o).getX()) {
            
            result = true;
        }
        
        return result;
    }
}
